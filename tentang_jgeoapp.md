# Tentang Journal Geoaplika (jgeoapp)

## Umum

File ini berisi berbagai hal yang mendeskripsikan pengelolaan [Journal Geoaplika](http://journals.itb.ac.id/index.php/geoaplika/). Kami dari JGeoApp melekatkan [lisensi CC-BY](https://creativecommons.org/share-your-work/public-domain/cc0/) kepada materi ini. Para pengelola jurnal yang lain dapat menggunakannya atau memodifikasinya. Kami hanya minta agar turut menyebarkan nilai-nilai sains terbuka kepada khalayak ramai. Untuk pertanyaan, Anda dapat menghubungi saya via surel `dasaptaerwin3 at gmail dot com`.

Journal Geoaplika adalah jurnal ilmiah berjenis "impact neutral" yang dikelola oleh [KK Geologi Terapan](https://medium.com/eco-hydrology-of-cikapundung), [Fakultas Ilmu dan Teknologi Kebumian](https://fitb.itb.ac.id/),  Institut Teknologi Bandung. "Neutral impact" atau dampak netral di sini  didefinisikan bahwa kami menerbit seluruh artikel riset pada berbagai  tahapan tanpa melakukan penilaian terhadap proyeksi dampaknya kepada  ilmu pengetahuan.

Menurut kami, keutamaan dari ilmu pengetahuan  adalah saat disebarkan. Sementara berbagai riset di Indonesia ini banyak  yang mengungkap potensi lokal, sehingga dampaknyapun (terkesan)  bersifat lokal pula. Dengan alasan ini, banyak jurnal menolaknya. Banyak  diantaranya dikirimkan ke jurnal LN (luar negeri) dan ditolak pada  kesempatan pertama.

Riset tidak harus berdampak secara nasional,  karena setiap riset akan memiliki keunikan yang diawali dengan keunikan  daerah atau obyek riset yang ditelaah.

Walaupun bersifat "impact  neutral", tapi kami sepakat bahwa makalah perlu menarik dan menonjolkan  sisi kemanfaatannya. Jurnal ini akan menampilkan artikel-artikel yang  memperlihatkan penerapan ilmu geosains dan interaksinya dengan bidang  ilmu lainnya untuk mengatasi berbagai masalah di masyarakat.

Kami mendefinisikan jurnal ini sebagai "jurnal sains terbuka" karena kami tidak memiliki misi lain, kecuali untuk menjadi media: publikasi, berlatih dan komunikasi para peneliti.

## Lingkup journal (journal's focus and scope)

Journal Geoaplika (JGeoApp) menerima artikel yang berkaitan dengan aplikasi bidang geosains (ilmu kebumian) untuk masyarakat, yang terdiri dari artikel bidang **geologi, meteorologi, kelautan, dan geodesi pada berbagai tahapan riset**. 

### Berdasarkan jenis riset

Dua kategori artikel yang dapat dikirimkan kepada kami:

- **Kategori Hasil Riset Universal (HRU)**, yaitu: berbagai artikel yang bersumber dari riset umum yang akan, sedang atau telah selesai dilakukan. 
- **Kategori Prakarsa Riset Nusantara (PRN)**, yaitu: berbagai artikel yang bersumber dari riset yang mengangkat keunikan alam nusantara atau dilaksanakan dalam skema program institusi (perguruan tinggi, Kemristekdikti, LPDP, dll), yang akan, sedang, atau telah selesai dilaksanakan.  

### Berdasarkan aplikasi riset

Karena bergerak di bidang aplikasi geosains, maka kami mendorong berbagai artikel dengan sub bidang berikut ini untuk dikirimkan kepada kami:

- pendekatan teknologi untuk geosains, misal: aplikasi sumber terbuka (*open source*), disain alat data logger, 
- pendekatan humaniora dalam geosains, misal: persepsi masyarakat tentang air bersih, persepsi masyarakat tentang bencana kebumian, kebijakan tanggap bencana, dan aplikasi humaniora lainnya kepada ilmu geosains,
- disain rekayasa dalam geosains: berupa gambar disain enjiniring (DED) suatu bangunan yang berkaitan dengan fitur-fitur geosains, disain arsitektur lansekap yang berkaitan elemen alam,
- naskah akademik regulasi terkait geosains,
- geowisata,
- dll (akan ditambahkan)

### Berdasarkan tahapan riset

Kami menerima berbagai makalah dalam berbagai tahapan riset:

1. **Proposal atau ide riset**: bagi para peneliti yang ingin mengklaim ide risetnya sejak awal,
2. **Ulasan literatur (*literature review*)**: bagi Anda yang telah menghabiskan berjam-jam menelaah ratusan makalah yang menjadi fokus riset Anda,
3. **Makalah data (*data paper*)**: bagi para peneliti yang menganggap datanya berharga dan berharap dapat berkolaborasi dengan pembaca, 
4. **Makalah metode**: metode sering kali perlu juga didiskusikan, karena mengandung hal baru dan mungkin juga potensi kesalahan. Ini untuk Anda yang ingin mendiskusikan metode, sebelum benar-benar digunakan dalam riset,
5. **Komunikasi ringkas (*short communications*)**: untuk para peneliti yang perlu segera menyampaikan hasil risetnya secara parsial, 
6. **Makalah riset lengkap (*full research article*)**: format makalah konvensional yang melaporkan berbagai hasil riset setelah semua tahapan selesai dilaksanakan.

## Telaah sejawat

### Proses

Jurnal ini memiliki keunikan yang tidak dimiliki jurnal lainnya, yakni:

- jurnal ini bersifat `dampak netral` (*impact neutral*): kami memiliki persepsi yang berbeda tentang dampak (*impact*). Dampak nyata akan dirasakan oleh pembaca, karena itu ukurannya akan bersifat kualitatif. Penulis perlu melakukan survey *tracer study* untuk mengukurnya. Jumlah sitasi artikel bukan merupakan indikator langsung dampak keilmuan. Oleh karenanya, kami tidak akan menggunakan jumlah sitasi dari artikel yang kami terbitkan untuk mengukur dampak. Penerbitan JGeoApp akan lebih mengutamakan  keunikan isi dan pesan yang disampaikannya.
- jurnal ini juga mengimplementasikan `telaah pasca publikasi` (*post publication review*). Seluruh artikel akan menjalani pemeriksaan awal sebelum akhirnya dapat langsung tayang di laman jurnal. Tahap selanjutnya adalah tahap telaah sejawat, yang akan diberikan oleh penelaah yang kami tugaskan dan penelaah publik.
- telaah yang kami lakukan adalah `telaah terbuka` (*open review*), penulis dan penelaah akan saling mengetahui identitas masing-masing. Kami berharap cara ini dapat meningkatkan transparansi dan kualitas hasil telaah. Di JGeoApp, kami mengutamakan proses telaah sejawat sebagai proses komunikasi, bukan pengadilan.

### Format

Formulir telaah sejawat dapat disampaikan dalam berbagai cara dan format sebagai berikut:

- formulir melalui aplikasi daring [Authorea](https://www.authorea.com/334755/U6hotwihUcNntFHE-hqb3A)
- formulir dalam format [docx](https://osf.io/2k5nd)
- formulir dalam format [odt](https://osf.io/7vm2n)

### Penyampaian formulir telaah sejawat

Formulir hasil telaah dapat dikirimkan melalui surel dan lampiran kepada kami. Ini kami lakukan, karena tidak semua pakar memiliki waktu untuk melakukan registrasi dan mengirimkan hasil telaah melalui sistem OJS kami.

### Waktu telaah

Rata-rata waktu yang kami alokasikan untuk proses telaah sejawat adalah tiga (3) minggu. Kami akan berupaya keras untuk memenuhi target tersebut. Bila sekiranya pakar yang ditugasi tidak mampu menyelesaikannya dalam tiga minggu, maka kami akan melakukan penggantian pakar.

### Pengingat 

Kami akan mengingatkan para pakar yang ditugasi untuk menelaah makalah dengan berbagai cara: 

- surel yang dibuat oleh sistem OJS
- surel pribadi yang dilakukan secara langsung oleh penyunting
- komunikasi lain yang memungkinkan (misal: sms, WA, Telegram, dll)

### Penilaian hasil telaah

Kami akan menilai hasil telaah dengan kriteria sebagai berikut:

- apakah hasil telaah telah menunjukkan aspek positif dari makalah secara jelas?
- apakah hasil telaah telah menunjukkan aspek yang perlu diperbaiki dari makalah secara jelas? 
- apakah hasil telaah layak untuk dipublikasikan melalui saluran-saluran seperti [Publons](publons.com) atau [INArxiv](inarxiv.id) (atas persetujuan penelaah dan penulis)?

## Pernyataan kerahasiaan

Nama-nama dan alamat surel yang telah ada dalam sistem OJS **hanya akan digunakan untuk keperluan pengelolaan jurnal**.

## Pengarsipan jurnal

Kami perlu berkonsultasi lebih dahulu dengan pengelola Jurnal ITB.

## Basis data pakar penelaah potensial

Kami sedang menyusun basis data ini.

## Panduan pengiriman makalah

### Panduan umum

**Kami sangat berhati-hati dalam menilai duplikasi dan plagiarisme**.

Prinsip utama kami adalah bahwa jurnal bukanlah satu-satunya media publikasi riset. Jadi kami tidak bermaksud memonopoli alur publikasi riset. Untuk itu JGeoApp juga mendorong penulis untuk mengirimkan artikel yang sebelumnya telah diunggah sebagai "**preprint**" (dokumen pra-*peer review*) atau dokumen tugas akhir daring (*e-these/e-dissertation*), atau artikel yang dikembangkan dari dokumen abstrak (**bukan abstrak panjang**/*extended abstract*) atau slide presentasi yang sebelumnya pernah dipresentasikan dalam suatu seminar. **Kami tidak menganggap dokumen-dokumen di bawah ini sebagai duplikasi**: - -

- preprint,
- abstrak (bukan abstrak panjang), 
- slide presentasi,
- skripsi/tesis/disertasi elektronik (*e-thesis*)

### Tahapan pengecekan

1. Lihat panduan umum di atas untuk jenis-jenis dokumen yang bukan duplikasi. Untuk itu, penulis perlu menyatakan status makalah dalam surat pengantar,
2. Makalah dikirimkan dalam format [Open/LibreOffice](Libreoffice.org), Microsoft Word, RTF, atau LATEX via [Overleaf](overleaf.com),
3. Gaya selingkung (*citation style*) mengikuti file `*.csl` yang disertakan bersama template. Semua rujukan diharuskan memiliki tautan daring (`url`), 
4. Kerangka makalah. Kerangka makalah minimum dapat mengikut panduan sebagai berikut.

#### Proposal/ide riset
......

#### Telaah literatur (*literature review*)
......

#### Komunikasi ringkas (*short communications*)
......

#### Makalah data (*data paper*)
......

#### Makalah metode (*method paper*)
......

#### Makalah instrumentasi (*instrument paper*)
......

#### Makalah riset lengkap (*full research article*)
Ikuti file panduan.


## Akses dan pengaturan keamanan

### Kebijakan akses terbuka

Jurnal ini menyediakan media akses terbuka ke publik untuk seluruh artikel dan komponen pendukungnya. Misi kami:
1. menyebarkan ilmu pengetahuan untuk kemanusiaan dengan menghilangkan dinding yang membatasi antara penulis dan pembaca, dengan menyediakan artikel dengan akses terbuka secara penuh. 
2. untuk itu, kami tidak menerbitkan `Perjanjian Pengalihan Hak Cipta` (*Copyright Transfer Agreement*)
3. karena itu kami menggunakan lisensi [CC-BY International 4.0](https://creativecommons.org/licenses/by/4.0/legalcode). Namun demikian, penulis dapat mengusulkan lisensi lainnya yang setara, seperti [MIT License](https://opensource.org/licenses/MIT), [GNU General Public License](https://opensource.org/licenses/gpl-license), atau [Mozilla Public License](https://opensource.org/licenses/MPL-2.0).

## Jadual penerbitan

Jurnal ini dibangun atas dasar peraturan baku berkala tercetak (printed periodicals), namun demikian, JGeoApp berkomitmen untuk memaksimumkan fitur-fitur jurnal digital daring dengan mempercepat proses publikasi tanpa menunggu jadwal penerbitan jurnal.

## Ketentuan tentang Hak Cipta

**Artikel Anda tetap milik Anda**

Di Journal Geoaplika, kami tidak menerbitkan `perjanjian pemindahan hak cipta` (*Copyright Transfer Agreement*). Artikel Anda tetaplah milik Anda. Lisensi seluruh artikel yang akan kami terbitkan adalah  [CC-BY Intl 4.0](https://creativecommons.org/licenses/by/4.0/legalcode). Anda dan pembaca dapat menyebarkan ulang, menggunakan  artikel, memodifikasi artikel, selama disebutkan sumbernya. Kami hanya mohon agar penulis dan pembaca turut menyebarkan nama JG dan nilai-nilainya. Namun demikian, bila diinginkan, penulis dapat mengusulkan **lisensi lainnya yang setara**, seperti [MIT License](https://opensource.org/licenses/MIT), [GNU General Public License](https://opensource.org/licenses/gpl-license), atau [Mozilla Public License](https://opensource.org/licenses/MPL-2.0).

## Penjelasan tambahan 

Jurnal ini memiliki keunikan yang tidak dimiliki jurnal lainnya, yakni:

- jurnal ini bersifat dampak netral (*impact neutral*): kami memiliki persepsi yang berbeda tentang dampak (*impact*). Dampak nyata akan dirasakan oleh pembaca. Oleh karenanya penerbitan artikel tidak hanya bergantung kepada proyeksi dampak yang akan terjadi, tetapi juga keunikan isi dan pesan yang disampaikannya.
- jurnal ini juga mengimplementasikan telaah pasca publikasi (*post publication review*). Seluruh artikel akan menjalani pemeriksaan awal sebelum akhirnya dapat langsung tayang di laman jurnal. Tahap selanjutnya adalah tahap telaah sejawat, yang akan diberikan oleh penelaah yang kami tugaskan dan penelaah publik.
- telaah yang kami lakukan bersifat terbuka (*open review*), penulis akan diminta menyertakan identitas lengkapnya. Penelaah akan mengetahui identitas penulis dan juga sebaliknya. Kemi harapkan cara ini dapat meningkatkan transparansi dan kualitas hasil telaah.

## *Copyediting*

Tahap *copyediting* bertujuan untuk meningkatkan kejelasan aliran informasi dan penceritaan makalah, yang sangat bergantung kepada kejernihan arti, struktur, dan penggunakan kosa kata. Tahapan ini merupakan tahapan terakhir bagi penulis untuk dapat menjelaskan substansi makalah agar sesuai dengan tujuan yang diharapkan. Tahap selanjutnya adalah pengecekan kesalahan ketik (*typos*) dan format. 

Proses penyuntingan kami lakukan menggunakan Open/Libre Office dan Microsoft Office yang mengikuti berbagai catatan dan komentar yang telah disusun oleh para penelaah yang kami tugaskan dan penelaah publik. Cara yang kami anjurkan adalah dengan mengaktifkan fitur `track changes` atau menggunakan layanan anotasi daring [Hypothes.is](Hypothes.is). Namun demikian, untuk kondisi umum, untuk memudahkan pelacakan  saran penelaah, serta perubahan dan tanggapan yang dilakukan oleh penulis, kami menyarankan cara sbb:  

1. **bagi penelaah:** tunjukkan no baris atau paragraf yang diberi saran dan tulisan saran diawali dengan `[saran]: .....` 
2. **bagi penulis:**
  - menanggapi saran perubahan:   Setiap tanggapan penulis, awali dengan `[tanggapan]: ...`,
  - penambahan:  beri awalan `[penambahan]:...` untuk setiap tambahan yang Anda lakukan,
  - penghapusan: beri awalan `[penghapusan]: ...` untuk setiap bagian yang Anda hapus. 
  - pemindahan: beri keterangan `[dipindahkan dari baris xx ke baris yy atau dari paragraf aa ke paragraf bb]: ...`.
## Penyuntingan tata letak (*Layout editor*)

Naskah JGeoApp diatur sedemikian rupa untuk memudahkan penulis. Template kami hanya terdiri dari satu kolom, menggunakan jenis huruf (*font*) yang mudah terbaca, yakni `Arial` dan yang sejenisnya. Kami juga telah menyediakan file pengatur gaya selingkung (csl). Silahkan memilih format file yang telah disediakan, `*.docx`, `*.odt`, atau `LATEX`. Kami sedang menyusun format `LATEX` via layanan daring [Overleaf](overleaf.com).