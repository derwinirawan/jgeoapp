# README

This project holds the contents, descriptions, and guidelines in running [Journal Geoaplika](http://journals.itb.ac.id/index.php/geoaplika), a reactivated OA journal from Institut Teknologi Bandung. This journal was first initiated by members of Applied Geology Research Group, Faculty of Earth Sciences and Technology, Institut Teknologi Bandung, 2007. It went dormant after two years of service.

Now we reactivated the journal using OJS-ITB platform and   bringing new values, the open science values. 

All materials are [CC-0](https://gitlab.com/derwinirawan/jgeoapp/blob/master/LICENSE.md), so journal managers could reuse and modify them to convey their missions. You could follow our [call for paper here](http://journals.itb.ac.id/index.php/geoaplika/announcement) or direct yourself to the [main file](https://gitlab.com/derwinirawan/jgeoapp/blob/master/tentang_jgeoapp.md). `#terbukaatautertinggal`

![Yes we're open - Wikimedia Commons - CC-0](https://upload.wikimedia.org/wikipedia/commons/9/93/Yes_We%27re_Open.jpg)

